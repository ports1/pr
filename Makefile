#
# PortRedirector Makefile.
#
# Anton Voronin (anton@urc.ac.ru)
# Makefile", line 30: Missing dependency operator

CP = cp -R -i -v
ECHO = echo
EXECPATH = /usr/bin:/usr/sbin:/bin:/sbin

PREFIX ?= /usr/local

all:

install:
	@-for file in * ; do if [ -d $$file ] ; then $(CP) $$file $(PREFIX) ; fi ; done
	@$(ECHO)
	@$(ECHO) 'Installation completed.'
	@$(ECHO)


